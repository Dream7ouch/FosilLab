export interface FosilExcelModel {
  NoFosil: number;
  Muestra: string;
  Grupo: string;
  Nombre: string;
  Pais: string;
  Estado: string;
  Municipio: string;
  Eon: string;
  Era: string;
  Epoca: string;
  Periodo: string;
  Formacion: string;
  Comentarios: string;
  Usuario_Alta: string;
  Usuario_Modifico: string;
}
